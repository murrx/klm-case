Front-end application
=====================
Please see https://bitbucket.org/murrx/klm-case-angular-front/src/master/ for the front-end angular application that works on this api


Travel API Client 
=================

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the assignment (after starting the application) go to:

[http://localhost:9000/travel/index.html](http://localhost:9000/travel/index.html)