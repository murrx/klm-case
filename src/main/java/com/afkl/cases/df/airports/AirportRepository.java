package com.afkl.cases.df.airports;

import com.afkl.cases.df.config.RestClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

@Repository
public class AirportRepository {
    private static final ParameterizedTypeReference<Resources<Airport>> AIRPORT_COLLECTION_TYPE = new ParameterizedTypeReference<Resources<Airport>>() {
    };
    private static final ParameterizedTypeReference<Airport> AIRPORT_TYPE = new ParameterizedTypeReference<Airport>() {
    };

    private final OAuth2RestTemplate restTemplate;
    private final RestClientProperties clientProperties;


    @Autowired
    public AirportRepository(OAuth2RestTemplate restTemplate, RestClientProperties clientProperties) {
        this.restTemplate = restTemplate;
        this.clientProperties = clientProperties;

    }

    @Async
    public CompletableFuture<Resources<Airport>> list() {
        URI uri = resourceUriBuilder().build().toUri();

        return CompletableFuture.supplyAsync(getResultFor(uri, AIRPORT_COLLECTION_TYPE));
    }


    @Async
    public CompletableFuture<Resources<Airport>> find(String term) {
        URI uri = resourceUriBuilder().query("term={keyword}").buildAndExpand(term).toUri();

        return CompletableFuture.supplyAsync(getResultFor(uri, AIRPORT_COLLECTION_TYPE));
    }

    @Async
    public CompletableFuture<Airport> get(String key) {
        URI uri = resourceUriBuilder().pathSegment(key).build().toUri();

        return CompletableFuture.supplyAsync(getResultFor(uri, AIRPORT_TYPE));
    }

    private UriComponentsBuilder resourceUriBuilder() {
        return UriComponentsBuilder.newInstance()
                .scheme(clientProperties.getScheme())
                .host(clientProperties.getHost())
                .port(clientProperties.getPort())
                .path(clientProperties.getAirportsEndpoint());
    }

    private <T> Supplier<T> getResultFor(URI uri, ParameterizedTypeReference<T> typeReference) {
        return () -> restTemplate.exchange(uri, HttpMethod.GET, null, typeReference).getBody();
    }

}
