package com.afkl.cases.df.airports;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Airport {

    private String code, name, description;

}
