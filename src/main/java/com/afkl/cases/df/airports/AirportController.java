package com.afkl.cases.df.airports;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.concurrent.ExecutionException;

@RestController()
@RequestMapping("/airports")
@CrossOrigin(origins = "http://localhost:4200")
public class AirportController {

    private final AirportRepository airportRepository;

    @Autowired
    public AirportController(AirportRepository airportRepository) {
        this.airportRepository = airportRepository;
    }

    @RequestMapping(value = "/{key}", method = RequestMethod.GET)
    public Airport show(@PathVariable("key") String key) throws ExecutionException, InterruptedException {
        return airportRepository.get(key).get();
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Airport> locations() throws ExecutionException, InterruptedException {
        return airportRepository.list().get().getContent();
    }

    @RequestMapping(method = RequestMethod.GET, params = "term")
    public Collection<Airport> find(@RequestParam("term") String term) throws ExecutionException, InterruptedException {
        return airportRepository.find(term).get().getContent();
    }
}
