package com.afkl.cases.df.fares;

import com.afkl.cases.df.airports.Airport;
import com.afkl.cases.df.airports.AirportRepository;
import com.afkl.cases.df.config.RestClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Repository
public class FareRepository {
    private final OAuth2RestTemplate restTemplate;
    private final RestClientProperties clientProperties;
    private final AirportRepository airportRepository;

    @Autowired
    public FareRepository(OAuth2RestTemplate restTemplate, RestClientProperties clientProperties, AirportRepository airportRepository) {
        this.restTemplate = restTemplate;
        this.clientProperties = clientProperties;
        this.airportRepository = airportRepository;
    }

    @Async
    public CompletableFuture<Fare> get(String origin, String destination) throws ExecutionException, InterruptedException {
        URI uri = resourceUriBuilder().pathSegment(origin).pathSegment(destination).build().toUri();

        CompletableFuture<Price> price = CompletableFuture.supplyAsync(() -> restTemplate.getForObject(uri, Price.class));
        CompletableFuture<Airport> originAirport = airportRepository.get(origin);
        CompletableFuture<Airport> destinationAirport = airportRepository.get(destination);

        CompletableFuture.allOf(price, originAirport, destinationAirport).join();

        Fare fare = new Fare();
        fare.setPrice(price.get());
        fare.setOrigin(originAirport.get());
        fare.setDestination(destinationAirport.get());

        return CompletableFuture.completedFuture(fare);
    }

    private UriComponentsBuilder resourceUriBuilder() {
        return UriComponentsBuilder.newInstance()
                .scheme(clientProperties.getScheme())
                .host(clientProperties.getHost())
                .port(clientProperties.getPort())
                .path(clientProperties.getFaresEndpoint());
    }
}
