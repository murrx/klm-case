package com.afkl.cases.df.fares;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController()
@RequestMapping("/fares")
@CrossOrigin(origins = "http://localhost:4200")
public class FareController {

    private final FareRepository fareRepository;

    @Autowired
    public FareController(FareRepository fareRepository) {
        this.fareRepository = fareRepository;
    }

    @RequestMapping(value = "/{origin}/{destination}", method = RequestMethod.GET)
    public Fare show(@PathVariable("origin") String origin, @PathVariable("destination") String destination) throws ExecutionException, InterruptedException {
        return fareRepository.get(origin, destination).get();
    }


}
