package com.afkl.cases.df.fares;

import com.afkl.cases.df.airports.Airport;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Fare {
    private Price price;
    private Airport origin, destination;
}
