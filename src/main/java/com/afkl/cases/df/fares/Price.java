package com.afkl.cases.df.fares;

import lombok.Data;

@Data
public class Price {
    private double amount;
    private String currency;
}
