package com.afkl.cases.df.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "rest-client")
@Getter
@Setter
public class RestClientProperties {

    private String scheme;
    private String host;
    private int port;

    private String airportsEndpoint;
    private String faresEndpoint;
}
